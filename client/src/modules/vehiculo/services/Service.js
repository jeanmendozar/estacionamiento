import Api from '@/services/Api'

export default {
  addIngreso (params) {
    return Api().post('/v1/vehiculos/addIngreso', params)
  },
  deleteVehicule (celda, placa) {
    return Api().delete('/v1/vehiculos/SalidaVehiculo/' + celda +'/'+ placa)
  }
}

import Parent from '@/modules/vehiculo/components/Parent'
import Todos from '@/modules/vehiculo/components/Todos'
// import Nuevo from '@/modules/vehiculo/components/Nuevo'
// import Editar from '@/modules/vehiculo/components/Editar'

export default [
  {
    path: '/vehiculo',
    component: Parent,
    children: [
      {
        path: '',
        name: 'allVehiculos',
        component: Todos
      }
      /* {
        path: 'new',
        name: 'NewVehiculo',
        component: Nuevo
      },
      {
        path: ':id',
        name: 'EditVehiculo',
        component: Editar
      } */
    ]
  }
]

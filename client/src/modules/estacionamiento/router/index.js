import Parent from '@/modules/estacionamiento/components/Parent'
import DashBoard from '@/modules/estacionamiento/components/DashBoard'

export default [
  {
    path: '/estacionamiento',
    component: Parent,
    children: [
      {
        path: '',
        name: 'EstacionamientoDashBoard',
        component: DashBoard
      }
    ]
  }
]

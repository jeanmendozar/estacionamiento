import Api from '@/services/Api'

export default {
  fetchCeldas () {
    return Api().get('v1/estacionamiento/get_celdas')
  },
  fetchEstacionamiento () {
    return Api().get('v1/estacionamiento/get_estacionamiento')
  }
}

import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/views/Hello'
import estacionamientoRoutes from '@/modules/estacionamiento/router'

Vue.use(Router)

const baseRoutes = [
  {
    path: '/',
    name: 'dashboard',
    component: Hello
  },
  {
    path: '*',
    redirect: { name: 'dashboard' }
  }
]

const routes = baseRoutes.concat(estacionamientoRoutes)

export default new Router({
  mode: 'history',
  routes
})

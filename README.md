# estacionamiento

### Steps to Run

1. Clone this repository https://jeanmendozar@bitbucket.org/jeanmendozar/estacionamiento.git

2. Navigate into the directory Estacionamiento

> cd your-path-to/estacionamiento

3. Build Docker Images

> docker-compose build

4. Run the stack :)

> docker-compose up

Your app should be running on (if using native docker).: 

http://localhost:8080


# Enunciado del Parqueadero Alegrate


Francisco desea montar un negocio de parqueadero y necesita un sistema para administrarlo.

El parqueadero cuenta con 20 celdas disponibles, éstas están distribuidos linealmente en donde se puede acomodar un vehículo delante de otro vehículo, como se puede observar en la siguiente imagen:

Francisco necesita una sistema que permita registrar el ingreso de un vehículo y le asigne una celda aleatoria.  

Cuando se registre la salida del vehículo se debe mostrar el valor a pagar contando los segundos que el vehículo estuvo ocupando la celda (el valor del parqueo es de 30 pesos por segundo), adicionalmente se debe registrar la venta creando una factura en Alegra con este valor.

#### ¿Cómo funciona?

* Francisco necesita registrar el ingreso de un vehículo, indicando la marca del vehículo y la placa/licence plate de éste.

* El sistema debe asignar al vehículo una celda aleatoria, tener en cuenta que si la celda de “atrás” se encuentra disponible, ésta debe ser la que se asigne al vehículo.

* Francisco debe poder registrar la salida de un vehículo, en donde se debe ingresar la placa/licence plate del vehículo que desea salir. Es posible que el vehículo que desea salir tenga otro obstruyéndole la salida, en este caso se debe asignar al vehículo que está obstruyendo otra celda, si hay disponible, si no, se debe asignar la celda del vehículo que desea salir. 

* Cuando el vehículo salga del parqueadero debe generarse la factura por el parqueo indicando los segundos que se ocupó la celda y el total a pagar (cada segundo cuesta 30 unidades), la factura se debe generar en Alegra


#### Francisco necesita observar en el sistema:

* La distribución de las celdas, mostrando las celdas disponibles y las ocupadas.

* Reporte de los vehículos que se encuentran actualmente en el parqueadero, mostrando la placa, la celda original a la cual ingresó el vehículo, la celda final y el tiempo transcurrido en el parqueadero

* Se necesita un reporte que indique las 5 celdas más ocupadas del parqueadero y los segundos que han sido ocupadas.

#### Consideraciones

* El tiempo para realizar la prueba es de 1 semana.

* Toda la comunicación con Alegra se debe hacer por medio de la API (https://developer.alegra.com/)

* Se debe enviar una url donde se pueda visitar la herramienta desarrollada.

* El código se debe alojar en Bitbucket en un repositorio de git privado. Se debe dar acceso al correo scouting@alegra.com.


La aplicación debe ser realizada en alguno de los siguientes frameworks: Laravel o Zend Framework (para backend) y VueJS, Angular (para frontend)

Se debe enviar también el correo y la contraseña de la cuenta de Alegra con la cual se hace la integración.



var express = require('express');
var router = express.Router();

router.use('/vehiculos', require('../modules/estacionamiento/routes/vehiculo'));
router.use('/estacionamiento', require('../modules/estacionamiento/routes/estacionamiento'));


module.exports = router;
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Vehiculo = mongoose.model('Vehiculo');

var HistorySchema = new Schema({
    celda: Number,
    vehiculo: { type: Schema.ObjectId, ref: "Vehiculo" } 
});

module.exports = mongoose.model('History', HistorySchema);

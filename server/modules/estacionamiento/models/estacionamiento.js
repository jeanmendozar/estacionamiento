var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Vehiculo = mongoose.model('Vehiculo');

var EstacionamientoSchema = new Schema({
    celda: Number,
    vehiculo: { 
        /*type: Schema.Types.ObjectId, 
        ref: 'Vehiculo'*/
        placa : String,
        marca :String
    },
});

module.exports = mongoose.model('Estacionamiento', EstacionamientoSchema);

const mongoose = require('mongoose');
const { Schema } = mongoose;

var VehiculoSchema = new Schema({
  marca: String,
  placa: String
});

module.exports = mongoose.model('Vehiculo', VehiculoSchema);

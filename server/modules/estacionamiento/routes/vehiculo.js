var express = require('express');
var router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
var vehiculo_controller = require('../controllers/vehiculo');
var estacionamiento_controller = require('../controllers/estacionamiento');

router.get('/', vehiculo_controller.default);
router.get('/crearEstacionamiento', estacionamiento_controller.crearEstacionamiento);
router.get('/get_celdas', estacionamiento_controller.get_celdas);
router.post('/addIngreso', vehiculo_controller.addIngreso);
router.delete('/SalidaVehiculo/:celda/:placa', vehiculo_controller.SalidaVehiculo);

module.exports = router;
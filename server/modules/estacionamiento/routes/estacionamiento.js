var express = require('express');
var router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
var estacionamiento_controller = require('../controllers/estacionamiento');

router.get('/crear', estacionamiento_controller.crearEstacionamiento);
router.get('/get_celdas', estacionamiento_controller.get_celdas);
router.get('/get_estacionamiento', estacionamiento_controller.get_estacionamiento);

module.exports = router;
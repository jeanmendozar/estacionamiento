var Estacionamiento = require("../models/estacionamiento");
var Vehiculo = require("../models/vehiculo");

exports.crearEstacionamiento = async (req, res) => {
	tamano_estacionamiento = 20;
	celdas = await Estacionamiento.find({})
	if(!celdas.length){
		for (var i = tamano_estacionamiento; i > 0; i--) {
			celda = await new Estacionamiento({celda:i, vehiculo : null});
	  		await celda.save();
		}
	}
}
exports.get_celdas = async (req, res) => {
	var estacionamiento = [{
		celdas: []
	},{
		celdas: []
	},{
		celdas: []
	},{
		celdas: []
	}]
	var fila = {
		celdas : []
	};
	var celdas = await Estacionamiento.find({},'celda vehiculo');
	celdas.forEach(function(doc, index) {
		if(index < 5)
			estacionamiento[0].celdas.push(doc);
		if(index >= 5 && index < 10)
			estacionamiento[1].celdas.push(doc);
		if(index >= 10 && index < 15)
			estacionamiento[2].celdas.push(doc);
		if(index >= 15 && index < 20)
			estacionamiento[3].celdas.push(doc);
	});
	res.json(estacionamiento);
}

exports.get_estacionamiento = async (req, res) => {
	const celdas = await Estacionamiento.find({ 'vehiculo.placa' : { $exists: true } },'celda vehiculo');	
	res.json(celdas);
}

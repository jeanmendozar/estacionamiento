var Vehiculo = require("../models/vehiculo");
var Estacionamiento = require("../models/estacionamiento");

exports.default = async (req, res) => {
  res.send({
    hello:'hello word from vehiculos'
  });
}

async function getCeldasDisponiblesbyFila () {
  disponibles = {'f20-16':[],'f15-11':[],'f10-6':[],'f5-1':[]};
  celdas = await Estacionamiento.find({ 'vehiculo.placa' : { $exists: false } },'celda vehiculo');
  celdas.forEach(function(doc, index) {
      if(doc.celda >= 16 && doc.celda <= 20)
        disponibles['f20-16'].push(doc.celda);
      if(doc.celda >= 11 && doc.celda <= 15)
        disponibles['f15-11'].push(doc.celda);
      if(doc.celda >= 6 && doc.celda <= 10)
        disponibles['f10-6'].push(doc.celda);
      if(doc.celda >= 1 && doc.celda <=5)
        disponibles['f5-1'].push(doc.celda);
  });
  return Promise.resolve(disponibles);
}

async function getCeldasDisponibles () {
  var BreakException = {};
  celdas = await Estacionamiento.find({},'celda vehiculo');
  //celdas = await Estacionamiento.find({ 'vehiculo.placa' : { $exists: false } },'celda vehiculo');
  disponibles = [];
  try {
     celdas.forEach(function(doc, index) {
        vehiculo = doc.vehiculo;
        if (typeof vehiculo.placa === 'undefined') {
            disponibles.push(doc.celda)
        }
        if(disponibles.length > 0){
          switch (doc.celda){
              case 16:
              case 11:
              case 6:
              case 1:
                 throw BreakException;
              break;
          }
        }
      });
  } catch (e) {
    return await Promise.resolve(disponibles);
  }
  return Promise.resolve(disponibles);
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


exports.addIngreso = async (req, res) => {
  //verificando que el vehiculo no exista en el estacionamiento
  verify_vehicle = await Estacionamiento.find({ 'vehiculo.placa' : { $eq: req.body.placa } });
  if(verify_vehicle.length){
          res.send({
              vehiculo_existe: true,
              message: 'Vehiculo existe DB!'
            })
  }
  else{
    disponibles = await getCeldasDisponibles();
    if(disponibles.length){
            var vehiculo = await Vehiculo.findOne({'placa':req.body.placa});
            if (!vehiculo){
            	vehiculo = await new Vehiculo(req.body);
            	await vehiculo.save();
            }
            //get Cel Random
            num = getRandomInt(0, disponibles.length -1);
            num_celda = ( disponibles.length > 1 ) ? disponibles[num] : disponibles[0];
            celda = await Estacionamiento.findOneAndUpdate(
                        {celda:num_celda},
                        { $set: {vehiculo : {placa: vehiculo.placa, marca: vehiculo.marca }}},
                        {new: true}
                        );
            res.send({
              lleno: false,
              disponibles: disponibles,
              message: 'Vehiculo saved successfully!'
            })
    }else{
      res.send({
        lleno: true,
        disponibles:disponibles,
        message: 'Estacionamiento Full'
      })
    }
  }

}

async function sacandoVehiculo(celda, disponibles) {
  casillas_libres = [];
  vehiculo_arriba = celda.vehiculo;
  vehiculo_abajo = {};
  c_abajo = (celda.celda > 5) ? celda.celda -5 : false;
  c_abajo_obj = await Estacionamiento.findOne({'celda':c_abajo});
  //bajo casillas si estan disponibles
  while(c_abajo > 0 && typeof c_abajo_obj.vehiculo.placa === 'undefined' ){
    c_abajo -= 5;
    c_abajo_obj = await Estacionamiento.findOne({'celda':c_abajo});
  }
        //reubicamos esta celda si tiene carro
        if (c_abajo_obj && typeof c_abajo_obj.vehiculo.placa !== 'undefined') {
            if(c_abajo_obj.celda >= 16 && c_abajo_obj.celda <= 20)
              casillas_libres = disponibles['f20-16'];
            if(c_abajo_obj.celda >= 11 && c_abajo_obj.celda <= 15)
              casillas_libres = disponibles['f15-11'];
            if(c_abajo_obj.celda >= 6 && c_abajo_obj.celda <= 10)
              casillas_libres = disponibles['f10-6'];
            if(c_abajo_obj.celda >= 1 && c_abajo_obj.celda <=5)
              casillas_libres = disponibles['f5-1'];
              //reubicamos aleatoriamente si tiene disponibilidad en su fila
              if(casillas_libres.length){
                //'la de abajo tiene disponibilida 
                vehiculo = celda.vehiculo;
                await Estacionamiento.findOneAndUpdate(
                          {celda:celda.celda},
                          { $set: {vehiculo : {}}},
                          {new: true}
                          );
                num = getRandomInt(0, casillas_libres.length -1);
                vehiculo_old = c_abajo_obj.vehiculo;
                await Estacionamiento.findOneAndUpdate(
                          {celda:c_abajo_obj.celda},
                          { $set: {vehiculo : vehiculo}},
                          {new: true}
                          );
                await Estacionamiento.findOneAndUpdate(
                          {celda:casillas_libres[num]},
                          { $set: {vehiculo : vehiculo_old}},
                          {new: true}
                          );
              }else{
                vehiculo_abajo = c_abajo_obj.vehiculo;
              }
              //intercambio position
              celda = await Estacionamiento.findOneAndUpdate(
                        {celda:celda.celda},
                        { $set: {vehiculo : vehiculo_abajo}},
                        {new: true}
                        );
              c_abajo_obj = await Estacionamiento.findOneAndUpdate(
                        {celda:c_abajo_obj.celda},
                        { $set: {vehiculo : vehiculo_arriba}},
                        {new: true}
                        );
          await sacandoVehiculo(c_abajo_obj, disponibles);
        }
      else{
        // salida limpia 
        await Estacionamiento.findOneAndUpdate(
                        {celda:celda.celda},
                        { $set: {vehiculo : {}}},
                        {new: true}
                        );
      }
    return Promise.resolve(true);
}

exports.SalidaVehiculo = async (req, res) => {
  var celda = await Estacionamiento.findOne({'celda':req.params.celda});
  disponibles = await getCeldasDisponiblesbyFila();
  await sacandoVehiculo(celda,disponibles);
  res.send({
      ready:true,
      celda:celda,
      disponibles:disponibles
    });
}